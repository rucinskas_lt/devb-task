var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var path = require('path');
var rename = require('gulp-rename');

/* ================ ASEMBLE ================ */
	var extname = require('gulp-extname');
	var assemble = require('assemble');
	var helpers = require('handlebars-helpers')();
	var prettify = require('gulp-html-prettify');
	var htmllint = require('gulp-htmllint')
	var htmlmin = require('gulp-htmlmin');

	var app = assemble();

	gulp.task('assemble:load', function(cb) {
		app.partials('templates/partials/*.hbs');
		app.layouts('templates/layouts/*.hbs');
		app.pages('templates/pages/*.hbs');
		app.data(require('./templates/data.json'));
		app.helpers(helpers);
		cb();
	});

	gulp.task('assemble:htmllint', function() {
		return gulp.src(['templates/partials/*.hbs', 'templates/layouts/*.hbs', 'templates/pages/*.hbs'])
			.pipe(plumber())
			.pipe(htmllint({
				rules: {
					"indent-style": false,
					"indent-width-cont": true,
					"attr-bans": [],
					"tag-bans": false,
					"attr-name-style": false,
					"line-end-style": false,
					"id-class-style": false,
					"attr-req-value": "",
					"img-req-alt": false,
					"tag-close": false,
					"tag-self-close": false
				}
			}, function htmllintReporter(filepath, issues) {
				if (issues.length > 0) {
					issues.forEach(function (issue) {
						if (["indent-style", "spec-char-escape", "img-req-src", "label-req-for"].indexOf(issue.rule) == -1) {
							console.log('=======================================');
							console.log(path.basename(filepath));
							console.log(issue);
						}
					});
			 
					process.exitCode = 1;
				}
			}));
	});

	gulp.task('assemble', ['assemble:load', 'assemble:htmllint'], function() {
		return app.toStream('pages')
			.pipe(plumber())
			.pipe(app.renderFile())
			.pipe(extname())
			.pipe(prettify({indent_char: '	', indent_size: 1}))
			.pipe(htmlmin({collapseWhitespace: true}))
			.on('error', console.log.bind(console))
			.pipe(app.dest('dist'));
	});

	gulp.task('assemble-watch', ['assemble'], reload);

/* ================ CSS ================ */
	var postcss = require('gulp-postcss');
	var sass = require('gulp-sass');
	var autoprefixer = require('autoprefixer');
	var cssnano = require('gulp-cssnano');
	var noComments = require('postcss-discard-comments');

	gulp.task('css', function() {
		gulp.src('./sass/**/*.scss')
			.pipe(plumber())
			.pipe(sourcemaps.init())
			.pipe(sass().on('error', sass.logError))
			.pipe(postcss([
				autoprefixer({browsers: ['last 20 version']}),
				noComments
			]))
			// .pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('dist/css'))

			.pipe(cssnano({zindex: false}))
			.pipe(rename({ extname: '.min.css' }))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('dist/css'));
	});

	gulp.task('css-watch', ['css'], reload);

/* ================ SCRIPTS ================ */
	var jshint = require('gulp-jshint');
	var concat = require('gulp-concat');
	var minifyjs = require('gulp-js-minify');
	var addsrc = require('gulp-add-src');
	var runSequence = require('run-sequence');

	gulp.task('js:modules', function() {
		return gulp.src(['./js/modules/*.js'])
			.pipe(jshint())
			.pipe(sourcemaps.init())
			.pipe(jshint.reporter('default'))
			.pipe(minifyjs())

			.pipe(addsrc.prepend('js/main-before.js'))
			.pipe(addsrc.prepend('js/vendors.js'))
			.pipe(addsrc.append('js/main-after.js'))

			.pipe(concat('main.js'))

			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('./dist/js/'));
	});

	gulp.task('js:vendors', function() {
		return gulp.src(['./js/vendors/*.js'])
			.pipe(concat('vendors.js'))
			.pipe(gulp.dest('./js/'));
	});

	gulp.task('js:main', function(done) {
		runSequence('js:vendors', 'js:module', function() {
			done();
		});
	});

	gulp.task('js:vendors-watch', ['js:vendors'], reload);
	gulp.task('js:modules-watch', ['js:modules'], reload);
	gulp.task('js:main-watch', ['js:main'], reload);

/* ================ WATCH ================ */
	var browserSync = require('browser-sync').create();
	
	gulp.task('watch', function () {
	    browserSync.init({
	        server: {
	            baseDir: "./dist"
	        }
	    });

		gulp.watch('sass/**/*.scss', ['css-watch']);
		gulp.watch('modules/**/*.js', {cwd: 'js'}, ['js:modules-watch']);
		gulp.watch('vendors/**/*.js', {cwd: 'js'}, ['js:vendors-watch']);
		gulp.watch(['js/main-before.js', 'js/main-after.js'], ['js:main-watch']);

		gulp.watch([
			'templates/partials/*.hbs',
			'templates/layouts/*.hbs',
			'templates/data.json',
		], ['assemble-watch']);

		var w2 = gulp.watch('templates/pages/*.hbs', ['assemble-watch']);

		w2.on('change', function (event) {
			if (event.type === 'deleted') {
				var fileName = path.basename(event.path);
				var destFilePath = path.resolve("dist", fileName);
				del(destFilePath.replace(".hbs", ".html"));
			}
		});
	});

/* ================ DEFAULT ================ */
	gulp.task('default', ['watch']);

	function reload(done) {
		setTimeout(function () {
			browserSync.reload();
			done();
		}, 500);
	}