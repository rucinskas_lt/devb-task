(function(){
  var burger = document.getElementById('header__burger');
  var nav = document.getElementById('site-nav');

  function handleMobileNav(e) {
    e.preventDefault();

    if (this.classList.contains('header__burger--open')) {
      this.classList.remove('header__burger--open');
      nav.classList.remove('nav--open');
    } else {
      this.classList.add('header__burger--open');
      nav.classList.add('nav--open');
    }
  }

	burger.addEventListener('click', handleMobileNav, false);
})();