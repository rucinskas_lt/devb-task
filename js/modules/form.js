(function(){
  var form = document.getElementById('main-form');
  var message = form.querySelector('.site-aside__form-message');

  var inputs = form.querySelectorAll('input');

  function handleSubmit(e) {
    e.preventDefault();

    if (form.firstname.value === '' || form.lastname.value === '') {
      message.innerHTML = 'Please fill required fields.';
      error();
    } else {
      pseudoSuccess();
    }
  }

  function error() {
    for (var i = 0; i < inputs.length; i++) {
      var input = inputs[i];

      if (input.hasAttribute('required') && input.value === '') {
        input.classList.add('error');
      } else {
        input.classList.remove('error')
      }
    }
  }

  function pseudoSuccess() {
    message.innerHTML = 'Your message has been sent.';

    for (var i = 0; i < inputs.length; i++) {
      var input = inputs[i];

      input.value = '';
      input.classList.remove('error');
    }
  }

	form.addEventListener('submit', handleSubmit, false);
})();